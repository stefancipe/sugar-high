import java.util.ArrayList;
import java.util.TreeMap;
import java.util.TreeSet;

public class SugarHigh {

	public static int[] sugarHigh(int [] candies, long treshold) {
		TreeSet<Integer> toRet = new TreeSet<Integer>();
		TreeMap<Integer,ArrayList<Integer>> treemap = new TreeMap<Integer,ArrayList<Integer>>();
		for(int i = 0; i < candies.length; i++) {
			if(treemap.containsKey(candies[i])) {
				treemap.get(candies[i]).add(i);
			} else {
				treemap.put(candies[i], new ArrayList<Integer>());
				treemap.get(candies[i]).add(i);
			}
		}
		int sumGrams = 0;
		boolean flag = false;
		for (Integer grams: treemap.keySet()) {
			for(Integer index : treemap.get(grams)) {
				if (sumGrams + grams > treshold ) {
					flag = true;
					break;
				}
				sumGrams+=grams;
				toRet.add(index);
			}
			if (flag) break;
		}
		Integer[] arrIntHelp = toRet.toArray(new Integer[toRet.size()]);
		int [] toRetArr = new int[toRet.size()];
		for(int i = 0; i < toRet.size(); i++) {
			toRetArr[i] = arrIntHelp[i].intValue();
		}
		return toRetArr;
	}
}
