# Sugar High

Code and description for Java Sugar High assignment for Prime Holding internship. The application is started by executing the main method from Main.java, but there's also a <b><a href="https://trinket.io/java/311eb0b1fd">trinket</a></b> with this code. So, the fastest way to start the application is to simply click on the trinket link and have the code executed in your browser right away.

<h1>Code explained</h1>
<h2>Main.java</h2>
Application entry point. Contains main function with the required data input from the examples. 
<h2>SugarHigh.java</h2>
Contains static method sugarHigh that receives as parameters int array of candies' amount of sugar in grams and int variable representing the treshold. The return value is an array of sorted indices of candies that the person can eat and not get sugar high. The algorithm is quite simple, it takes advantage of TreeMap, TreeSet and ArrayList structures. First thing first, the TreeSet instance helps us store the indices and sorts them as well. Next one is TreeMap instance with Integer key (grams of sugar) and ArrayList of Integers (indices of candies list that contain that amount of grams of sugar). This way, we have sorted the grams of sugar (TreeMap sorts the keys) while also keeping the lists of indices (they are sorted as well because of the way indices were traversed in the for loop). <br>           
We traverse this TreeMap, adding the indices to TreeSet until we reach the treshold or we run out of indices to add. Noting once again that the TreeSet sorts the elements on insertion, only thing left to do is to convert the TreeSet to int array and to return the said array. <br>
>>>
Your main priority is to eat the maximum number of candies possible, but if there are multiple ways of doing this, choose the one with the fewest grams of sugar total. If there's still a tie, choose the lower indices.
>>>
Both of these requirements are met by the algorithm since it starts with elements with the least amount of sugar, and with those that have smaller index.