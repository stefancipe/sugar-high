public class Main {

	public static void main(String[] args) {
		System.out.println("Example 1: candies = [33, 20, 12, 19, 29] and threshold = 33");	
		int arrInFirst[] = {33, 20, 12, 19, 29};
 		int arrOutFirst[] = SugarHigh.sugarHigh(arrInFirst,33);
 		System.out.println(java.util.Arrays.toString(arrOutFirst));
 		
 		System.out.println("\nExample 2: candies = [62, 67, 100] and threshold = 8");	
		int arrInSecond[] = {62, 67, 100};
 		int arrOutSecond[] = SugarHigh.sugarHigh(arrInSecond,8);
 		System.out.println(java.util.Arrays.toString(arrOutSecond));
 		
 		System.out.println("\nExample 3: candies = [16, 39, 67, 16, 38, 71] and threshold = 17");	
		int arrInThird[] = {16, 39, 67, 16, 38, 71};
 		int arrOutThird[] = SugarHigh.sugarHigh(arrInThird,17);
 		System.out.println(java.util.Arrays.toString(arrOutThird));
 		
 		System.out.println("\nExample 4: candies = [16, 3, 14, 17, 11] and threshold = 99");	
		int arrInFourth[] = {16, 3, 14, 17, 11};
 		int arrOutFourth[] = SugarHigh.sugarHigh(arrInFourth,99);
 		System.out.println(java.util.Arrays.toString(arrOutFourth));

		System.out.println("\nExample 5: candies = [5, 3, 7, 1, 2, 2, 4, 1, 7, 2, 4] and threshold = 13");	
		int arrInFifth[] = {5, 3, 7, 1, 2, 2, 4, 1, 7, 2, 4};
 		int arrOutFifth[] = SugarHigh.sugarHigh(arrInFifth,16);
 		System.out.println(java.util.Arrays.toString(arrOutFifth)); 
	}
}